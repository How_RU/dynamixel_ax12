
#include "dynamixel_ax12.h"

//комманда в цикле отправляющая сообщения по uart.
void			ft_send_command(unsigned char id, unsigned char len, unsigned char arr[100])
{

}


// считываем и проверяем значения из очереди. Также расчитывается crc. Если
// последовательность проходит проверку, то передаем значения по UART
void			ft_command_output(void)
{
	unsigned char	i = 0;					//
	unsigned char	arr[MAXSIZE] = {255};	// Массив из
	unsigned char	len;					//
	unsigned char	temp;					//
	unsigned char	id;						//
	unsigned char	crc;					//

	if (!ft_isempty() && (print_front() == 255))
		pop();
	else
	{
		ft_queue_error();
		return ;
	}
	if (!ft_isempty() && (print_front() == 255))
		pop();
	else
	{
		ft_queue_error();
		return ;
	}
	if (!ft_isempty())
		id = pop();
	else
	{
		ft_queue_error();
		return ;
	}
	if (!ft_isempty())
		len = pop();
	else
	{
		ft_queue_error();
		return ;
	}

	temp = len;
	while (!ft_isempty() && temp > 0)
	{
		arr[i++] = pop();
		temp--;
	}
	if (temp > 0)
	{
		ft_queue_error();
		return ;
	}
	crc = id + len;
	i = 0;
	while (i < len)
		crc += arr[i++];
	crc = 255 - (crc % 256);
	ft_send_command(id, len, arr);
}
