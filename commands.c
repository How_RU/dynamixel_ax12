

#include "dynamixel_ax12.h"

void	start_bits(void)
{
	push(255);
	push(255);
}

void	ft_write_id(unsigned char id, unsigned char new_id)
{
	if (ft_free_space() < 7 || new_id > 253)
		return ;
	start_bits();
	push(id);
	push(4);
	push(WRITE_DATA);
	push(ID);
	push(new_id);
}

// val - абсолютное значение желаемой скорости, например 115200
void	ft_write_baud_rate(unsigned char id, uint32_t val)
{
	if (val > 1e6 || ft_free_space() < 7)
		return ;
	start_bits();
	push(id);
	push(4);
	push(WRITE_DATA);
	push(BAUD_RATE);
	switch (val)
	{
		case 1000000:
			push (1);
			break;
		case 500000:
			push (3);
			break;
		case 115200:
			push (16);
			break;
		case 19200:
			push (103);
			break;
		case 9600:
			push (207);
			break;
	}
}

void	ft_write_delay_time(unsigned char id, unsigned char val)
{
	if (val > 254 || ft_free_space() < 7)
		return ;
	start_bits();
	push(id);
	push(4);
	push(WRITE_DATA);
	push(DELAY_TIME);
	push(val);
}

void	ft_write_cw_angle_lim(unsigned char id, uint16_t val)
{
	if (val > 1023 || ft_free_space() < 8)
		return ;
	start_bits();
	push(id);
	push(5);
	push(WRITE_DATA);
	push(CW_ANGLE_LIM);
	push(val & 255);
	push(val >> 8);
}

void	ft_write_ccw_angle_lim(unsigned char id, )
{

}

void	ft_write_highest_temp(unsigned char id, )
{

}

void	ft_write_lowest_volt(unsigned char id, )
{

}

void	ft_write_highest_volt(unsigned char id, )
{

}

void	ft_write_max_torque(unsigned char id, )
{

}

void	ft_write_status_return_level(unsigned char id, )
{

}

void	ft_alarm_led(unsigned char id, )
{

}

void	ft_alarm_shutdown(unsigned char id, )
{

}

void	ft_torque_enable(unsigned char id, )
{

}

void	ft_led(unsigned char id, )
{

}

void	ft_cw_compliance_margin(unsigned char id, )
{

}

void	ft_ccw_compliance_margin(unsigned char id, )
{

}

void	ft_cw_compliance_slope(unsigned char id, )
{

}

void	ft_goal_position(unsigned char id, )
{

}

void	ft_moving_speed(unsigned char id, )
{

}

void	ft_torque_limit(unsigned char id, )
{

}

void	ft_registered_instruction(unsigned char id, )
{

}

void	ft_lock(unsigned char id, )
{

}

void	ft_punch(unsigned char id, )
{

}

void	ft_ping(unsigned char id)
{
	if (ft_free_space() < 5)
		return ;
	start_bits();
	push(id);
	push(2);
	push(1);
}

// для чтения из динамикселей нужно указать адрес начала считывания и количество считанных байт
// пример - счтить текущую температуру из ID = 0w. ft_read_data(0, PRESENT_TEMPERATURE, LEN_PRESENT_TEMPERATURE);
void	ft_read_data(unsigned char id, char start_addr, char len)
{
	if (ft_free_space() < len + 5)
		return ;
	start_bits();
	push(id);
	push(len + 2);
	push(READ_DATA);
	push(start_addr);
	push(len);
}
