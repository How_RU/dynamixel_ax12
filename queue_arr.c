
#include <stdio.h>
#include "dynamixel_ax12.h"

unsigned char	queue[MAXSIZE];

char	front = -1;
char	rear = -1;
char	size = -1;

char			ft_isempty(void)
{
	return (size < 0);
}

char			ft_isfull(void)
{
	return (size == MAXSIZE);
}

unsigned char	ft_free_space(void)
{
	return (MAXSIZE - (size == -1) ? 0 : size);
}

void			push(int value)
{
	if (size < MAXSIZE)
	{
		if (size < 0)
		{
			queue[0] = value;
			front = rear = 0;
			size = 1;
		}
		else if (rear == MAXSIZE - 1)
		{
			queue[0] = value;
			rear = 0;
			size++;
		}
		else
		{
			queue[rear+1] = value;
			rear++;
			size++;
		}
	}
	else
		printf("Queue is full\n");
}

unsigned char	pop(void)
{
	char	temp = front;

	if (size < 0)
	{
		printf("Queue is empty\n");
		return (-1);
	}
	size--;
	front++;
	return (queue[front]);
}

unsigned char	return_front(void)
{
	return (queue[front]);
}

void			print_queue(void)
{
	if (rear>=front)
	{
		for (int i = front; i <= rear; i++)
			printf("%d\n",queue[i]);
	}
	else
	{
		for (int i = front; i < MAXSIZE; i++)
			printf("%d\n",queue[i]);
		for (int i = 0; i <= rear; i++)
			printf("%d\n",queue[i]);
	}
}

void			ft_queue_error(void)
{
	printf("Error in queue\n");
	while (!ft_isempty() && return_front() != 255)
		pop();
}

/*
int main()
{

	push(4);
	push(8);
	push(10);
	push(20);
	print_queue();
	pop();
	printf("After dequeue\n");
	print_queue();
	push(50);
	push(60);
	push(70);
	push(80);
	pop();
	push(90);
	push(100);
	push(110);
	push(120);
	push(130);
	push(140);
	pop();
	push(150);
	pop();
	push(160);
	pop();
	push(0xff);
	printf("After enqueue\n");
	print_queue();
	return 0;
}
*/
