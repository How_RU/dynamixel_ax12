//
// Created by dr-monro on 29.01.2019.
//

#include <cstring>
#include "BusReciever.h"
#include "BusDevice.h"

#define MAXPARAMETERLEN 8
#define COMMONADDRESS (0xFE)


BusReciever::BusReciever(BusDevice *parent,uint8_t &address,uint8_t max_len):_parent(parent),_address(address),_max_len(max_len)
{
    reset_parcer_state(&this->parserState);
};


void BusReciever::reset_parcer_state(BusReciever::ParcerState *p_state)
{
    p_state->state = WAIT_FOR_START1;
    p_state->counted_bytes= 0;
    p_state->wait_len= 0;
    p_state->count_params = 0;
    p_state->checksum = 0;
}

uint8_t BusReciever::parce_bytes(uint8_t byte)
{
    return parce_bytes(byte,&(this->parserState));
}

uint8_t BusReciever::parce_bytes(uint8_t byte,ParcerState *p_state)
{
    uint8_t is_finished = 0;
    input_buffer[p_state->counted_bytes]=byte;
    p_state->counted_bytes = p_state->counted_bytes + 1;
    switch (p_state->state)
    {
        case WAIT_FOR_START1:
            if (byte == 0xFF)
            {
                p_state->state = WAIT_FOR_START2;
            }
            else p_state->state = RESET;
            break;
        case WAIT_FOR_START2:
            if (byte == 0xFF)
            {
                p_state->state = GET_ID;
            }
            else p_state->state = RESET;
            break;
        case GET_ID:
            p_state->state =((byte == this->_address) || ((byte == COMMONADDRESS))) ? GET_LEN : RESET;
            break;
        case GET_LEN:
            p_state->wait_len = byte;
            p_state->state = (byte>MAXPARAMETERLEN) ? RESET : GET_ERROR;

            break;
        case GET_ERROR:
            p_state->state = GET_PARAMETERS;
            break;
        case GET_PARAMETERS:
            if (p_state->count_params<(p_state->wait_len-2)-1)
            {
                p_state->count_params += 1;
            }
            else p_state->state = GET_CHECKSUM;
            break;
        case GET_CHECKSUM:
            p_state->checksum = byte;
            uint8_t checsum = calcCRC(input_buffer+2,(p_state->counted_bytes-1-2));
            if (checsum == p_state->checksum)
            {
//                if(_parent != nullptr)
//                    _parent->changed(this,input_buffer);
                p_state->state = RESET;
                is_finished = 1;
            }

            else p_state->state = RESET;
            // printf("get it \n");
            // else p_state->state = RESET;
            break;
    }
    if ((p_state->state) == RESET)
        reset_parcer_state(p_state);
    return is_finished;
}

uint8_t BusReciever::calcCRC(uint8_t *bytes, uint8_t len)
{
    uint8_t calc_checksum = 0;
    for(uint8_t i = 0;i<len;i++)
        calc_checksum += bytes[i];
    calc_checksum = ~calc_checksum;
    return calc_checksum;
}


