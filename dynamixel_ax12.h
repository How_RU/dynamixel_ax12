


#ifndef DYNAMIXEL_AX12_H
#define DYNAMIXEL_AX12_H

// Перечень доступных инструкций
#define PING		1
#define READ_DATA	2
#define WRITE_DATA	3
#define REG_WRITE	4
#define ACTION		5
#define RESET		6
#define SYNC_WRITE	0x83


//Перечень адресов в памяти динамикселя
#define MODEL_NUM				0 // Read Only
#define VERSION_OF_FIRMWARE		2 // Read Only
#define ID						3
#define BAUD_RATE				4
#define DELAY_TIME				5
#define CW_ANGLE_LIM			6
#define CCW_ANGLE_LIM			8
#define HIGHEST_TEMP_LIM		11
#define LOWEST_VOLT_LIM			12
#define HIGHEST_VOLT_LIM		13
#define MAX_TORQUE				14
#define STATUS_RETURN_LEVEL		16
#define ALARM_LED				17
#define ALARM_SHUTDOWN			18
#define DOWN_CALIBRATION		20 // Read Only
#define UP_CALIBRATION			22 // Read Only
#define TORUQE_ENABLE			24
#define LED						25
#define CW_COMPLIANCE_MARGIN	26
#define CCW_COMPLIANCE_MARGIN	27
#define CW_COMPLIANCE_SLOPE		28
#define CCW_COMPLIANCE_SLOPE	29
#define GOAL_POSITION			30
#define MOVING_SPEED			32
#define TORQUE_LIMIT			34
#define PRESENT_POSITION		36 // Read Only
#define PRESENT_SPEED			38 // Read Only
#define PRESENT_LOAD			40 // Read Only
#define PRESENT_VOLTAGE			42 // Read Only
#define PRESENT_TEMPERATURE		43 // Read Only
#define REGISTERED_INSTRUCTION	44
#define MOVING					46 // Read Only
#define LOCK					47
#define PUNCH					48

#define LEN_MODEL_NUM				2
#define LEN_VERSION_OF_FIRMWARE		1
#define LEN_ID						1
#define LEN_BAUD_RATE				1
#define LEN_DELAY_TIME				1
#define LEN_CW_ANGLE_LIM			2
#define LEN_CCW_ANGLE_LIM			2
#define LEN_HIGHEST_TEMP_LIM		1
#define LEN_LOWEST_VOLT_LIM			1
#define LEN_HIGHEST_VOLT_LIM		1
#define LEN_MAX_TORQUE				2
#define LEN_STATUS_RETURN_LEVEL		1
#define LEN_ALARM_LED				1
#define LEN_ALARM_SHUTDOWN			1
#define LEN_DOWN_CALIBRATION		2
#define LEN_UP_CALIBRATION			2
#define LEN_TORUQE_ENABLE			1
#define LEN_LED						1
#define LEN_CW_COMPLIANCE_MARGIN	1
#define LEN_CCW_COMPLIANCE_MARGIN	1
#define LEN_CW_COMPLIANCE_SLOPE		1
#define LEN_CCW_COMPLIANCE_SLOPE	1
#define LEN_GOAL_POSITION			2
#define LEN_MOVING_SPEED			2
#define LEN_TORQUE_LIMIT			2
#define LEN_PRESENT_POSITION		2
#define LEN_PRESENT_SPEED			2
#define LEN_PRESENT_LOAD			2
#define LEN_PRESENT_VOLTAGE			1
#define LEN_PRESENT_TEMPERATURE		1
#define LEN_REGISTERED_INSTRUCTION	1
#define LEN_MOVING					1
#define LEN_LOCK					1
#define LEN_PUNCH					2

#define MAXSIZE 100 // размер последовательности, в которой сохраняем байты перед отправкой на устройство

void			push(int value);
unsigned char	pop(void);

char			ft_isempty(void);
char			ft_isfull(void);
unsigned char	ft_free_space(void);
unsigned char	print_front(void);

void			ft_command_output(void);
void			ft_queue_error(void);
void			print_queue(void);


#endif
